import sympy as sy

z = sy.Symbol('z')

M1 = sy.Matrix([
    [1,0,0,-24],
    [0,1,0,-7],
    [0,0,1,-2],
    [0,0,0,0]]) 

M2 = sy.Matrix([
    [0,4,6,-40],
    [0,0,4,-8],
    [0,0,0,0],
    [0,0,0,0]])
    
M3 = sy.Matrix([
    [0,0,16,32],
    [0,0,0,0],
    [0,0,0,0],
    [0,0,0,0]])

M4 = sy.Matrix([
    [0,0,0,24],
    [0,0,0,7],
    [0,0,0,2],
    [0,0,0,1]])

decomposition = M1/(z-2) + M2/(z-2)**2 + M3/(z-2)**3 + M4 / (z-4)
sy.pprint(sy.simplify(decomposition))

B = sy.Matrix([
    [2,4,6,8],
    [0,2,4,6],
    [0,0,2,4],
    [0,0,0,4]])

sy.pprint(sy.simplify(decomposition*(z*sy.eye(4)-B)))