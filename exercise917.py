import numpy as np
from matplotlib import pyplot as plt

def expected_loss(tau, LFP, LFN, lambduh):
    x = (1-lambduh)*tau
    x -= np.sqrt((1-lambduh) * tau * lambduh * (1-tau))
    gross_repeated_term = x/(tau-lambduh)
    prob_FP_given_Y0 = (1-gross_repeated_term)**3
    prob_Y0 = (1-lambduh)
    expected_FP_loss = LFP * prob_Y0 * prob_FP_given_Y0
    prob_FN_given_Y1 = gross_repeated_term**3
    prob_Y1 = lambduh
    expeted_FN_loss = LFN * prob_Y1 * prob_FN_given_Y1
    return expected_FP_loss + expeted_FN_loss
    
    
lambduh = 1/2
LFP = 1
LFN = 2
eqn923_tau = LFP / (LFP + LFN)
tau = np.linspace(0, 1, 1000)

plt.plot(tau, expected_loss(tau, LFP, LFN, lambduh))
plt.plot(eqn923_tau, expected_loss(eqn923_tau, LFP, LFN, lambduh), 'ro', label="Equation 9.23's minimizer")
plt.show()
    
    
    