import numpy as np

Q = np.array([
    [0, 1, 1/2,   0, 0, 0],
    [0, 0,   0, 1/2, 0, 0],
    [0, 0,   0, 1/2, 0, 0],
    [1, 0,   0,   0, 0, 1],
    [0, 0, 1/2,   0, 0, 0],
    [0, 0,   0,   0, 1, 0]
    ])
    
pi = np.ones(6)/6

for n in [0, 1, 2, 3, 4, 5]:
    marginal_distribution = np.linalg.matrix_power(Q, 10**n).dot(pi)
    print("This is the marginal distribution for 10^{}:".format(n))
    print(marginal_distribution)
    print()