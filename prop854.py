"""
prop854.py
"""

import numpy as np
from scipy import stats

#Define random variables
left_endpoint = -1
length = 2
Z0 = stats.uniform(left_endpoint, length)

a = np.sqrt(5)/4
length = 1/2
left_endpoint = a - length/2
Z1_abs = stats.uniform(left_endpoint, length)

B = stats.bernoulli(1/2)

#Take samples
n = 10**8

z0 = Z0.rvs(n)

z1_abs = Z1_abs.rvs(n)
z1_sign = (B.rvs(n)*2 - 1)
z1 = z1_abs * z1_sign

#print sample statistics
print("Sample mean of Z0:", z0.mean())
print("Sample mean of Z1:", z1.mean())
print("Sample variance of Z0:", z0.var())
print("Sample variance of Z1:", z1.var())

#Define our function.
f = lambda x : x**2

#Print out sample statistics of f applied to samples.
print("Sample mean of f(Z0):", f(z0).mean())
print("Sample mean of f(Z1):", f(z1).mean())
print("Sample variance of f(Z0):", f(z0).var())
print("Sample variance of f(Z1):", f(z1).var())